"""Tests for the skelly.builder.console.git module."""

import os
import subprocess
from typing import Optional

import pytest

from skelly.builder.console.git import init_git


def get_branch(repo: str) -> Optional[str]:
    """Get the current branch of a repo."""
    res = subprocess.run(
        ["git", "status", "--porcelain=v2", "--branch"],
        cwd=repo,
        capture_output=True,
        check=True,
    )
    lines = res.stdout.split(b"\n")
    for line in lines:
        if line.startswith(b"# branch.head "):
            _, _, branch, *_ = line.split()
            if branch:
                return branch.decode()
    return None


class TestGit:
    """Test suite for the git module."""

    @staticmethod
    def test_init_default(tmp_path: str) -> None:
        """Inits a repo with the default branch name."""
        init_git(tmp_path)
        assert os.path.isdir(f"{tmp_path}/.git")
        assert get_branch(tmp_path) == "main"

    @staticmethod
    @pytest.mark.parametrize("branch", ["main", "master", "dev"])
    def test_init_branch(branch: str, tmp_path: str) -> None:
        """Inits a repo with the given branch name."""
        init_git(tmp_path, branch)
        assert os.path.isdir(f"{tmp_path}/.git")
        assert get_branch(tmp_path) == branch
