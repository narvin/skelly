"""Tests for the args module."""

import os
from typing import Iterable, Mapping

import pytest

from skelly.args import Args, PromptSpec, parse_args, prompt_args


class TestParseArgs:
    """Test suite for parse_args function."""

    @staticmethod
    def test_defaults(monkeypatch: pytest.MonkeyPatch) -> None:
        """Defaults are all set when no options are passed."""
        monkeypatch.setattr("sys.argv", ["skelly"])
        args = parse_args()
        assert args.builder == "default"
        assert args.builder_opts == {}
        assert args.template_path is None
        assert args.name == os.path.basename(os.getcwd())
        assert args.branch == "main"
        assert args.no_git is False
        assert args.silent is False
        assert args.target == os.getcwd()

    @staticmethod
    @pytest.mark.parametrize(
        "args, name",
        [
            (["skelly", "-n", "foo"], "foo"),
            (["skelly", "--name", "foo"], "foo"),
            (["skelly", "-n", "foo", "/tmp/mypkg"], "foo"),
            (["skelly", "--name", "foo", "/tmp/mypkg"], "foo"),
            (["skelly", "/tmp/mypkg"], "mypkg"),
            (["skelly", "-n", "foo"], "foo"),
            (["skelly", "--name", "foo"], "foo"),
            (["skelly"], os.path.basename(os.getcwd())),
        ],
    )
    def test_name_derived_from_target(
        args: Iterable[str], name: str, monkeypatch: pytest.MonkeyPatch
    ) -> None:
        """name, if not specified, is the basename of the target directory."""
        monkeypatch.setattr("sys.argv", args)
        parsed_args = parse_args()
        assert parsed_args.name == name

    @staticmethod
    @pytest.mark.parametrize(
        "args, opt, opt_dict",
        [
            (["skelly", "-b", "foo", "bar"], "builder_opts", {"foo": "bar"}),
            (["skelly", "--builder-opt", "foo", "bar"], "builder_opts", {"foo": "bar"}),
            (
                ["skelly", "-b", "foo", "bar", "-b", "baz", "quo"],
                "builder_opts",
                {"foo": "bar", "baz": "quo"},
            ),
            (
                ["skelly", "-b", "foo", "bar", "--builder-opt", "baz", "quo"],
                "builder_opts",
                {"foo": "bar", "baz": "quo"},
            ),
            (["skelly", "-t", "foo", "bar"], "template_vars", {"foo": "bar"}),
            (
                ["skelly", "--template-var", "foo", "bar"],
                "template_vars",
                {"foo": "bar"},
            ),
            (
                ["skelly", "-t", "foo", "bar", "-t", "baz", "quo"],
                "template_vars",
                {"foo": "bar", "baz": "quo"},
            ),
            (
                ["skelly", "-t", "foo", "bar", "--template-var", "baz", "quo"],
                "template_vars",
                {"foo": "bar", "baz": "quo"},
            ),
        ],
    )
    def test_opt_dicts(
        args: Iterable[str],
        opt: str,
        opt_dict: Mapping[str, str],
        monkeypatch: pytest.MonkeyPatch,
    ) -> None:
        """Append options that take 2 arguments produce dicts."""
        monkeypatch.setattr("sys.argv", args)
        parsed_args = parse_args()
        assert getattr(parsed_args, opt) == opt_dict

    @staticmethod
    @pytest.mark.parametrize(
        "args, pargs",
        [
            (
                ["skelly", "-B", "python", "/tmp/mypkg"],
                {"builder": "python"},
            ),
            (
                ["skelly", "-b", "foo", "bar", "-t", "baz", "quo", "-T", "~/tmpl"],
                {
                    "builder_opts": {"foo": "bar"},
                    "template_vars": {"baz": "quo"},
                    "template_path": "~/tmpl",
                    "name": os.path.basename(os.getcwd()),
                    "target": os.getcwd(),
                },
            ),
            (
                ["skelly", "-n", "foo", "-g", "master", "-s", "/tmp/mypkg"],
                {"name": "foo", "branch": "master", "silent": True},
            ),
        ],
        indirect=["pargs"],
    )
    def test_args(
        args: Iterable[str], pargs: Args, monkeypatch: pytest.MonkeyPatch
    ) -> None:
        """Return an Args object with parsed values."""
        monkeypatch.setattr("sys.argv", args)
        assert parse_args() == pargs


class TestPromptArgs:
    """Test suite for prompt_args function."""

    @staticmethod
    @pytest.mark.parametrize(
        "spec, partial_prompts",
        [
            ([(None, "foo", None)], ["foo"]),
            ([("Enter foo", "foo", None)], ["Enter foo"]),
            ([(None, "foo", None), ("Enter foo", "foo", None)], ["foo", "Enter foo"]),
        ],
    )
    def test_prompt_default(
        spec: PromptSpec,
        partial_prompts: Iterable[str],
        monkeypatch: pytest.MonkeyPatch,
    ) -> None:
        """The prompt, if not specified, defaults to the spec name."""
        prompts = []

        def mock_input(prompt: str) -> str:
            prompts.append(prompt)
            return ""

        monkeypatch.setattr("builtins.input", mock_input)
        prompt_args(spec)
        assert prompts == [f"{prompt}: " for prompt in partial_prompts]

    @staticmethod
    @pytest.mark.parametrize(
        "spec, res",
        [
            ([(None, "foo", "bar")], {"foo": "bar"}),
            ([("Enter foo", "foo", "bar")], {"foo": "bar"}),
            (
                [(None, "foo", "1"), ("Enter bar", "bar", "2")],
                {"foo": "1", "bar": "2"},
            ),
        ],
    )
    def test_value_default(
        spec: PromptSpec, res: Mapping[str, str], monkeypatch: pytest.MonkeyPatch
    ) -> None:
        """The value, if not specified, defaults to the spec default."""

        def mock_input(_: str) -> str:
            return ""

        monkeypatch.setattr("builtins.input", mock_input)
        assert prompt_args(spec) == res

    @staticmethod
    @pytest.mark.parametrize(
        "spec",
        [
            [(None, "foo", None)],
            [("Enter foo", "foo", None)],
            [(None, "foo", None), ("Enter bar", "bar", None)],
        ],
    )
    def test_value_omitted(spec: PromptSpec, monkeypatch: pytest.MonkeyPatch) -> None:
        """The value, if not specified, and has no default, is omitted."""

        def mock_input(_: str) -> str:
            return ""

        monkeypatch.setattr("builtins.input", mock_input)
        assert prompt_args(spec) == {}

    @staticmethod
    @pytest.mark.parametrize(
        "spec, inputs, res",
        [
            ([(None, "foo", None)], ["quo"], {"foo": "quo"}),
            ([(None, "foo", "bar")], ["quo"], {"foo": "quo"}),
            ([("Enter foo", "foo", None)], ["quo"], {"foo": "quo"}),
            ([("Enter foo", "foo", "bar")], ["quo"], {"foo": "quo"}),
            (
                [(None, "foo", None), ("Enter bar", "bar", None)],
                ["5", "6"],
                {"foo": "5", "bar": "6"},
            ),
            (
                [(None, "foo", "1"), ("Enter bar", "bar", "2")],
                ["5", "6"],
                {"foo": "5", "bar": "6"},
            ),
        ],
    )
    def test_value(
        spec: PromptSpec,
        inputs: Iterable[str],
        res: Mapping[str, str],
        monkeypatch: pytest.MonkeyPatch,
    ) -> None:
        """Returns a dict of prompted values."""

        mut_inputs = list(inputs)

        def mock_input(_: str) -> str:
            return mut_inputs.pop(0)

        monkeypatch.setattr("builtins.input", mock_input)
        assert prompt_args(spec) == res
