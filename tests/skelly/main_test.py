"""Tests for the main module."""

from __future__ import annotations

import pytest

from skelly.args import Args
from skelly.main import main


class TestMain:
    """Test suite for the main function."""

    @staticmethod
    @pytest.mark.parametrize("pargs", [{"builder": "foo"}], indirect=True)
    def test_unregistered_builder_stderr(
        pargs: Args, capsys: pytest.CaptureFixture[str]
    ) -> None:
        """Prints error to stderr when passed an unregistered builder."""
        main(pargs)
        _, err = capsys.readouterr()
        assert (
            "builder 'foo' not registered: registered builders: default, python\n"
            == err
        )

    @staticmethod
    @pytest.mark.parametrize("pargs", [{"builder": "foo"}], indirect=True)
    def test_unregistered_builder_exit(pargs: Args) -> None:
        """Returns 1 when passed an unregistered builder."""
        assert main(pargs) == 1

    @staticmethod
    @pytest.mark.parametrize(
        "pargs", [{}, {"builder": "default"}, {"builder": "python"}], indirect=True
    )
    def test_call_builder(pargs: Args, monkeypatch: pytest.MonkeyPatch) -> None:
        """Loads a builder and calls its build method."""

        class MockBuilder:  # pylint: disable=too-few-public-methods
            """Mock builder."""

            init_count = 0
            build_count = 0
            args: list[Args] = []

            def __init__(self, args: Args) -> None:
                MockBuilder.init_count += 1
                MockBuilder.args.append(args)

            def build(self) -> None:
                """Mock build method."""
                MockBuilder.build_count += 1

        monkeypatch.setattr("builder_python.builder.PythonBuilder", MockBuilder)
        main(pargs)
        assert MockBuilder.init_count == 1
        assert MockBuilder.build_count == 1
        assert len(MockBuilder.args) == 1
        assert MockBuilder.args[0] is pargs
