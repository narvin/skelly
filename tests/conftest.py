"""pytest shared fixtures."""

import pytest
from _pytest.fixtures import SubRequest

from skelly.args import Args


@pytest.fixture
def pargs(request: SubRequest) -> Args:
    """Make an Args instance with defaults for any values that aren't provided."""
    kwargs = request.param
    return Args(
        builder=kwargs.get("builder", "default"),
        builder_opts=kwargs.get("builder_opts", {}),
        template_path=kwargs.get("template_path", None),
        template_vars=kwargs.get("template_vars", {}),
        name=kwargs.get("name", "mypkg"),
        branch=kwargs.get("branch", "main"),
        no_git=kwargs.get("no_git", False),
        silent=kwargs.get("silent", False),
        target=kwargs.get("target", "/tmp/mypkg"),
    )
