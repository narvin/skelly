"""Tests for the skelly.builder.console.git module."""

from __future__ import annotations

import os
import subprocess
import sys
import tempfile
from typing import Generator, Optional, Tuple

import pytest
from _pytest.fixtures import SubRequest

from skelly.builder.python.venv import create_venv


def get_version(python_bin: str) -> Optional[str]:
    """Get the Python version."""
    res = subprocess.run([python_bin, "--version"], capture_output=True, check=True)
    lines = res.stdout.split(b"\n")
    for line in lines:
        if line.startswith(b"Python "):
            _, version, *_ = line.split()
            if version:
                return version.decode()
    return None


def has_pkgs(python_bin: str, pkgs: list[str]) -> bool:
    """Checks if the pkgs are all installed for the python_bin."""
    res = subprocess.run(
        [python_bin, "-m", "pip", "list"], capture_output=True, check=True
    )
    lines = res.stdout.split(b"\n")[2:]
    installed_pkgs = [line.split()[0] for line in lines if line]
    return all(bytes(pkg, "utf-8") in installed_pkgs for pkg in pkgs)


def count_pkgs(python_bin: str) -> int:
    """Checks if the pkgs are all installed for the python_bin."""
    res = subprocess.run(
        [python_bin, "-m", "pip", "list"], capture_output=True, check=True
    )
    lines = res.stdout.split(b"\n")[2:]
    return len([line for line in lines if line])


def pkg_editable_loc(python_bin: str, pkg: str) -> Optional[str]:
    """Returns the location of an editably installed package."""
    res = subprocess.run(
        [python_bin, "-m", "pip", "list", "-e"], capture_output=True, check=True
    )
    lines = res.stdout.split(b"\n")[2:]
    for line in lines:
        line_pkg, _, loc, *_ = line.split()
        if line_pkg.decode() == pkg and loc:
            return loc.decode()
    return None


class TestVenv:
    """Test suite for the venv module."""

    @staticmethod
    @pytest.fixture
    def dir_reqs(request: SubRequest, tmp_path: str) -> Tuple[str, list[str]]:
        """Return a mock project directory path with a requirements file."""
        reqs = request.param
        with open(f"{tmp_path}/requirements.txt", "w", encoding="utf-8") as req_fp:
            for req in reqs:
                req_fp.write(f"{req}\n")
        return tmp_path, reqs

    @staticmethod
    @pytest.fixture
    def file_reqs(request: SubRequest) -> Generator[Tuple[str, list[str]], None, None]:
        """Return the abs path to a requirements file in a new temp directory."""
        reqs = request.param
        with tempfile.NamedTemporaryFile(mode="w+t") as req_fp:
            for req in reqs:
                req_fp.write(f"{req}\n")
            req_fp.flush()
            yield req_fp.name, reqs

    @staticmethod
    @pytest.fixture
    def proj(tmp_path: str) -> str:
        """Return a mock project directory path with an installable package."""
        with open(f"{tmp_path}/pyproject.toml", "w", encoding="utf-8") as setup_fp:
            setup_fp.write(
                """
[build-system]
requires = ["setuptools>=42"]
build-backend = "setuptools.build_meta"
            """
            )
        with open(f"{tmp_path}/setup.cfg", "w", encoding="utf-8") as setup_fp:
            setup_fp.write(
                """
[metadata]
name = mypkg
version = 0.0.1

[options]
package_dir =
    = src
packages = find:

[options.packages.find]
where = src
            """
            )
        os.makedirs(f"{tmp_path}/src/mypkg")
        os.mknod(f"{tmp_path}/src/mypkg/__init__.py")
        os.mknod(f"{tmp_path}/src/mypkg/mypkg.py")
        return tmp_path

    @staticmethod
    def test_create_default_env_dir(tmp_path: str) -> None:
        """Creates a venv in the target in the default env_dir."""
        create_venv(tmp_path, req_file=None, install_dot=False)
        assert os.path.isdir(f"{tmp_path}/.venv")
        assert (
            get_version(f"{tmp_path}/.venv/bin/python")
            == f"{sys.version_info.major}"
            + f".{sys.version_info.minor}"
            + f".{sys.version_info.micro}"
        )

    @staticmethod
    @pytest.mark.parametrize("venv", [".venv", ".venv310"])
    def test_create_env_dir(venv: str, tmp_path: str) -> None:
        """Creates a venv in the target in the specified env_dir."""
        venv_path = f"{tmp_path}/{venv}"
        create_venv(tmp_path, venv_path, req_file=None, install_dot=False)
        assert os.path.isdir(venv_path)
        assert (
            get_version(f"{venv_path}/bin/python")
            == f"{sys.version_info.major}"
            + f".{sys.version_info.minor}"
            + f".{sys.version_info.micro}"
        )

    @staticmethod
    @pytest.mark.parametrize(
        "dir_reqs", [["skelly"], ["skelly", "pylint"]], indirect=True
    )
    def test_create_default_req_file(dir_reqs: Tuple[str, list[str]]) -> None:
        """Creates a venv with packages from requirements.txt in the target."""
        proj_dir, reqs = dir_reqs
        create_venv(proj_dir, install_dot=False)
        assert has_pkgs(f"{proj_dir}/.venv/bin/python", reqs)
        assert count_pkgs(f"{proj_dir}/.venv/bin/python") >= 2 + len(reqs)

    @staticmethod
    @pytest.mark.parametrize(
        "file_reqs", [["skelly"], ["skelly", "pylint"]], indirect=True
    )
    def test_create_req_file(tmp_path: str, file_reqs: Tuple[str, list[str]]) -> None:
        """
        Creates a venv with packages from a requirements file in a different directory.
        """
        req_file, reqs = file_reqs
        create_venv(tmp_path, req_file=req_file, install_dot=False)
        assert has_pkgs(f"{tmp_path}/.venv/bin/python", reqs)
        assert count_pkgs(f"{tmp_path}/.venv/bin/python") >= 2 + len(reqs)

    @staticmethod
    def test_create_no_req_file(tmp_path: str) -> None:
        """Creates a venv with no additional packages."""
        create_venv(tmp_path, req_file=None, install_dot=False)
        assert count_pkgs(f"{tmp_path}/.venv/bin/python") <= 2

    @staticmethod
    def test_create_install_dot(proj: str) -> None:
        """
        Creates a venv in a project directory and installs the src packages in
        editable mode.
        """
        create_venv(proj, req_file=None, install_dot=True)
        assert has_pkgs(f"{proj}/.venv/bin/python", ["mypkg"])
        assert pkg_editable_loc(f"{proj}/.venv/bin/python", "mypkg") == f"{proj}"
        assert count_pkgs(f"{proj}/.venv/bin/python") >= 3
